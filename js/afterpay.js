if (typeof Afterpay === 'undefined') {
	var Afterpay = {};

	/**
	 * @see http://locutus.io/php/version_compare/
	 */
	Afterpay.versionCompare = function(v1, v2, operator) {
		var i
		var x
		var compare = 0

		var vm = {
			'dev': -6,
			'alpha': -5,
			'a': -5,
			'beta': -4,
			'b': -4,
			'RC': -3,
			'rc': -3,
			'#': -2,
			'p': 1,
			'pl': 1
		}

		var _prepVersion = function (v) {
			v = ('' + v).replace(/[_\-+]/g, '.')
			v = v.replace(/([^.\d]+)/g, '.$1.').replace(/\.{2,}/g, '.')
			return (!v.length ? [-8] : v.split('.'))
		}

		var _numVersion = function (v) {
			return !v ? 0 : (isNaN(v) ? vm[v] || -7 : parseInt(v, 10))
		}

		v1 = _prepVersion(v1)
		v2 = _prepVersion(v2)
		x = Math.max(v1.length, v2.length)
		for (i = 0; i < x; i++) {
			if (v1[i] === v2[i]) {
				continue
			}
			v1[i] = _numVersion(v1[i])
			v2[i] = _numVersion(v2[i])
			if (v1[i] < v2[i]) {
				compare = -1
				break
			} else if (v1[i] > v2[i]) {
				compare = 1
				break
			}
		}
		if (!operator) {
			return compare
		}

		switch (operator) {
			case '>':
			case 'gt':
				return (compare > 0)
			case '>=':
			case 'ge':
				return (compare >= 0)
			case '<=':
			case 'le':
				return (compare <= 0)
			case '===':
			case '=':
			case 'eq':
				return (compare === 0)
			case '<>':
			case '!==':
			case 'ne':
				return (compare !== 0)
			case '':
			case '<':
			case 'lt':
				return (compare < 0)
			default:
				return null
		}
	};

	Afterpay.loadScript = function(url, callback) {
		var script = document.createElement('script');
		script.type = 'text/javascript';
		if (script.readyState) { // I.E.
			script.onreadystatechange = function() {
				if (script.readyState == 'loaded' || script.readyState == 'complete') {
					script.onreadystatechange = null;
					callback();
				}
			};
		} else { // Others
			script.onload = function() {
				callback();
			};
		}
		script.src = url;
		document.getElementsByTagName('head')[0].appendChild(script);
	};

	Afterpay.launchPopup = function($, event) {
		event.preventDefault();

		var $popup_wrapper, $popup_outer, $popup_inner, $a, $img, $close_button, $currency;

		$popup_wrapper = $('#afterpay-popup-wrapper');
		$currency = $('#modal-window-currency').attr('currency');

		if ($popup_wrapper.length > 0) {
			$popup_wrapper.show();
		} else {
			$popup_wrapper = $(document.createElement('div'))
				.attr('id', 'afterpay-popup-wrapper')
				.css({
					'position': 'fixed',
					'z-index': 999999999,
					'left': 0,
					'top': 0,
					'right': 0,
					'bottom': 0,
					'overflow': 'auto'
				})
				.appendTo('body')
				.on('click', function(event) {
					Afterpay.closePopup($, event);
				});

			$popup_outer = $(document.createElement('div'))
				.attr('id', 'afterpay-popup-outer')
				.css({
					'display': 'flex',
					'justify-content': 'center',
					'align-content': 'center',
					'align-items': 'center',
					'width': '100%',
					'min-height': '100%',
					'background-color': 'rgba(0, 0, 0, 0.80)'
				})
				.appendTo($popup_wrapper);

			$popup_inner = $(document.createElement('div'))
				.attr('id', 'afterpay-popup-inner')
				.css({
					'position': 'relative',
					'background-color': '#fff'
				})
				.appendTo($popup_outer);

			$a = $(document.createElement('a'));

			if ($currency == 'USD') {
				$a.attr('href', 'https://www.afterpay.com/purchase-payment-agreement');
			} else {
				$a.attr('href', 'https://www.afterpay.com/terms');
			}

			$a.attr('target', '_blank')
				.css({
					'display': 'block'
				})
				.appendTo($popup_inner);

			$img = $(document.createElement('img'));

			if ($currency == 'USD') {
				if ($(window).width() > 640) {
					$img.attr('src', 'https://static.afterpay.com/us-popup-medium.png');
				} else {
					$img.attr('src', 'https://static.afterpay.com/us-popup-small.png');
				}
			} else {
				if ($(window).width() > 640) {
					$img.attr('src', 'https://static.afterpay.com/lightbox-desktop.png');
				} else {
					$img.attr('src', 'https://static.afterpay.com/lightbox-mobile.png');
				}
			}

			$img.css({
					'display': 'block',
					'width': '100%'
				})
				.appendTo($a)
				.on('click', function(event) {
					event.stopPropagation();
				});

			$close_button = $(document.createElement('a'))
				.attr('href', '#')
				.css({
					'position': 'absolute',
					'right': '8px',
					'top': '8px'
				})
				.html('<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" width="32px" height="32px"><g id="surface1"><path style=" " d="M 16 3 C 8.832031 3 3 8.832031 3 16 C 3 23.167969 8.832031 29 16 29 C 23.167969 29 29 23.167969 29 16 C 29 8.832031 23.167969 3 16 3 Z M 16 5 C 22.085938 5 27 9.914063 27 16 C 27 22.085938 22.085938 27 16 27 C 9.914063 27 5 22.085938 5 16 C 5 9.914063 9.914063 5 16 5 Z M 12.21875 10.78125 L 10.78125 12.21875 L 14.5625 16 L 10.78125 19.78125 L 12.21875 21.21875 L 16 17.4375 L 19.78125 21.21875 L 21.21875 19.78125 L 17.4375 16 L 21.21875 12.21875 L 19.78125 10.78125 L 16 14.5625 Z "/></g></svg>')
				.appendTo($popup_inner)
				.on('click', function(event) {
					Afterpay.closePopup($, event);
				});
		}
	};

	Afterpay.closePopup = function($, event) {
		event.preventDefault();

		$('#afterpay-popup-wrapper').hide();
	};

	Afterpay.init = function($) {
		$(document).on("click", "a[href='#afterpay-what-is-modal']", function(event){
			Afterpay.launchPopup($, event);
		});
	};

	if (typeof jQuery != 'function' || !Object.prototype.hasOwnProperty.call(jQuery, 'fn') || Afterpay.versionCompare(jQuery.fn.jquery, '1.7', '<')) {
		Afterpay.loadScript('https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js', function() {
			Afterpay.jQuery_1_12_4 = jQuery.noConflict(true);
			Afterpay.init(Afterpay.jQuery_1_12_4);
		});
	} else {
		Afterpay.init(jQuery);
	}
}
